defmodule MillionDb.Repo do
  use Ecto.Repo,
    otp_app: :million_db,
    adapter: Ecto.Adapters.Postgres
end
