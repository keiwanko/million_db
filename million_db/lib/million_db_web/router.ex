defmodule MillionDbWeb.Router do
  use MillionDbWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", MillionDbWeb do
    pipe_through :api
  end
end
